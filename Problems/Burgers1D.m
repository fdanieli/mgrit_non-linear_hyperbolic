function [f, df] = Burgers1D( u, ~, L, k, fType )
%Burgers1D right-hand side of (inviscid) Burgers' equation in 1D.
% 
% The Burgers' equation can be written (in conservative form) as:
%  dt(u)   = - dx( u^2/2 ).
% The domain is considered to be an interval of lenght L, where grid points
% are distributed uniformly. Periodic boundary conditions are applied
% (but other BC can be used by properly padding with ghost cells). Flux
% updates are given using a Lax Friedrichs scheme based on a WENO flux
% reconstruction routine OR using WENO reconstruction on the state plus a 
% Roe Riemann solver and a Godunov flux (LeVeque entropic fix applied).
%
%
% Syntax:  [f, df] = Burgers1D( u, t, L, k, fType )
%   Input:
%    u       - Matrix. Each column contains the state of the system at an
%               instant, specified in t, where the right-hand side needs to
%               be evaluated.
%    t       - Vector. Contains time instants at which the right-hand side
%               needs to be evaluated (unused).
%    L       - Scalar. Size of domain. (Optional, default: 1).
%    k       - Integer. Order of WENO polynomial reconstruction (Optional,
%               default: 0, corresponding to no reconstruction).
%    fType   - String. Type of flux employed in the scheme. Accepts two
%               values: LF for Lax-Friedrichs flux reconstruction, or ROE
%               for state reconstruction with Roe Riemann solver, LeVeque
%               entropic fix and Godunov flux. (Optional, default: LF).
%
%		Output:
%    f       - Matrix. Evaluations of f(u,t).
%    df      - (Possibly 3D) Matrix. Evaluations of jacobian of f, df(u,t).
%               UNSUPPORTED.
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% January 2019; Last revision: May-2019
%
%
%

if ( nargout > 1 )
	error('Burgers does not provide the Jacobian of the rhs (yet). It would be a bit of a pain to compute, after all...')
end

if (nargin<3 || isempty(L))
	L=1;
end

if (nargin<4 || isempty(k))
	k=0;
end

if (nargin<5 || isempty(fType))
	fType= 'LF';
end

% in case you want some fancy initial conditions for testing:
% x = (dx/2):dx:(L-dx/2);			% these are actually the cell *centers*
% u0 =   ( x <= L/2 )*1 + ( x > L/2 )*0;


% recover some relevant quantities
NX  = size(u,1);		% number of cells in domain
dx = L/NX;					% cell size



switch fType
	case 'LF'
		%% Lax-Friedrichs flux splitting
		% Lax-Friedrichs flux at interface is given by
		% F = ( F(uL)+F(uR) + q(uL-uR) ) / 2
		% we perform the splitting:
		% F = Fp + Fm = ( F(uL) + q*uL )/2  +  ( F(uR) - q*uR )/2
		% and we use upwinding on the flux: Fp is taken from values reconstructed
		% right of the interface, while Fm from values left of it. The total LF
		% flux across the interface is then recovered by summing Fp and Fm.
		
		% - Impose BC
		% - neumann
		% v =  [   flip( u(1:k+1,:) ,1) ;  u  ;   flip( u((end-k):end,:),1) ];
		% - dirichlet
		% v =  [ - flip( u(1:k+1,:) ,1) ;  u  ; - flip( u((end-k):end,:),1) ];	TO_TEST!!
		% - periodic (default in WENO)
		v = u;

% 		% define positive and negative fluxes
% 		q = max(abs(v));
% 		Fp = 0.5*( v.*v/2 + q*v );
% 		Fm = 0.5*( v.*v/2 - q*v );
% 
% 		% reconstruct positive flux for left and negative for right
% 		[ FpL, ~   ] = weno( Fp', k );
% 		[   ~, FmR ] = weno( Fm', k );
% 		F = FpL' + FmR';												% sum the two parts of the flux to recover total flux at interface
% 
% 		% eventually remove ghost cell evaluations
% 		if size(v,1) > size(u,1)
% 			F = F( (k+2):(end-(k+1)) );
% 		end

		[ vL, vR ] = weno( v', k );
		vL = vL'; vR = vR';

		% find maximum eig for both left/right states
		q = max( abs( [ vL; vR ] ) );
		
		% compute fluxes left and right of interface
		FL = vL.*vL / 2;
		FR = vR.*vR / 2;
		
		% compute total flux through interface
		F = ( FL+FR + q * ( vL- vR ) ) / 2; 

		% eventually remove ghost cell evaluations
		if size(v,1) > size(u,1)
			F = F( (k+2):(end-(k+1)) );
		end
		
		
	case 'ROE'
		%% State reconstruction + Roe Riemann solver
		% The Roe scheme works by solving a Riemann problem at the cells interface.
		% Rather than solving the actual Non-linear Riemann problem arising from
		% the set of Euler equations, though, this problem is linearised around an
		% approriate averaged state. This is called Roe average, and the relevant
		% average quantities are defined as:
		% - v_avg = 1/2*( vR*vR - vL*vL ) / ( vR - vL ) = ( vR + vL )/2

		% - Impose BC and reconstruct state left and right of each cell interface
		% using WENO
		% - neumann
		% v =  [   flip( u(1:k+1,:) ,1) ;  u  ;   flip( u((end-k):end,:),1) ];
		% [ vL, vR ] = weno( v', k );
		% vL = vL( (k+2):(end-(k+1))  )'; vR = vR( (k+2):(end-(k+1)) )';
		% - dirichlet
		% v =  [ - flip( u(1:k+1,:) ,1) ;  u  ; - flip( u((end-k):end,:),1) ];	TO_TEST!!
		% [ vL, vR ] = weno( v', k );
		% vL = vL( (k+2):(end-(k+1))  )'; vR = vR( (k+2):(end-(k+1)) )';
		% - periodic (default in WENO)
		[ vL, vR ] = weno( u', k );
		vL = vL'; vR = vR';

		% compute Roe averages at interface
		vBar = 0.5 * ( vR + vL );


		% Once the average state is provided, the problem solved is actually:
		% vt + vBar*vx = 0
		% The solution to such a linear problem is very easy to recover. In fact,
		% the eigenvalue vBar identifies a contact discontinuity, which splits the
		% space-time domain in 2 different zones: the solution there is constant,
		% equal to vL on the left and vR on the right.

		% The Godunov flux is then given by
		%  FI = ( f(vL)+f(vR) )/2  - ( |vBar|* (vR-vL) ) /2

		% The problem with linear approximate Riemann solver is that they only
		% consider contact discontinuities (they're linear after all). While this
		% is fine most times, it becomes a bit weird when a transonic rarefaction
		% fan appears in the original problem. In this case, the state within the
		% fan (in particular, that at x=0) needs to be reconstructed with more
		% care. To this purpose, a so-called "entropy fix" is applied. See
		% [1] "A review of entropy fixes as applied to Roe's linearization",
		%     M. Pelanti, L. Quartapelle, L. Vigevano
		%     (https://www.researchgate.net/publication/267418915_A_review_of_entropy_fixes_as_applied_to_Roe's_linearization)

		% - Apply entropic fix
		% (Second entropy fix of Harten and Hyman)
		%  - compare them with the average eigenval using:
		%   d = max( 0, vBar-vL,             vR-vBar )
		d = max(max( 0, vBar-vL), vR-vBar );
		%  - substitute |vBar| with q(vBar)
		absVBar = abs( vBar );
		d( d == 0 ) = absVBar( d == 0 ) - 1;	% these are just to avoid dividing by 0...
		q = absVBar.*( absVBar >= d ) + 0.5 * ( vBar.*vBar./d + d ).*( absVBar < d );
		% (Leveque fix)
		% q = abs( vBar );
		% rarIdx  = logical( ( vL<0 ).* ( vR>0 ) );
		% q( rarIdx ) = ( ( vR(rarIdx) + vL(rarIdx) ).*vBar(rarIdx) - 2*vR(rarIdx).*vL(rarIdx)  )...
		%						 ./ ( vR(rarIdx) - vL(rarIdx) );
		% notice the expression simplifies dramatically for Burgers:
		% q = ( vL-vR ) / 2,
		% but I'm leaving it in general form just because...
		% Or just don't apply any entropic fix at all
		% q = abs( vBar );

		% - Compute fluxes
		%  - flux left and right of interfaces
		FL = 0.5*vL.*vL;
		FR = 0.5*vR.*vR;
		%  - Godunov flux at interface
		F = (       FR + FL )  /2 ...
			- ( q.* ( vR - vL ) )/2;
		
	otherwise
		error('Burger1D: Flux type prescribed not recognised. Only valid tags are LF or ROE')
end


% either case, the total flux within the cell is given by 
f = ( F(1:end-1,:) - F(2:end,:) ) / dx;



end