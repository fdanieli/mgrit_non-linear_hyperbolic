function [f, df] = Burgers1D_LF_corr( u, ~, L, dt, OMatch, lvl )
%Burgers1D_LF_corr right-hand side of (inviscid) Burgers' equation in 1D, including corrections for fine integrator matching.
% 
% The Burgers' equation can be written (in conservative form) as:
%  dt(u)   = - dx( u^2/2 ).
% The domain is considered to be an interval of lenght L, where grid points
% are distributed uniformly. Periodic boundary conditions are applied.
% Flux updates are given using a classical Lax-Friedrichs flux definition.
% This function is intended to be used in an MGRIT iteration, together with
% a Forward Euler time stepper: it corrects the classical LF update by
% adding terms that better match the action of the fine integrator at a
% finer level.
% This function is handy but quite dirty, so handle with care.
%
%
% Syntax: [f, df] = Burgers1D_LF_corr( u, ~, L, dt, OMatch, lvl )
%
%   Input:
%    u       - Matrix. Each column contains the state of the system at an
%               instant, specified in t, where the right-hand side needs to
%               be evaluated.
%    t       - Vector. Contains time instants at which the right-hand side
%               needs to be evaluated (unused).
%    L       - Scalar. Size of domain.
%    dt      - Scalar. Time discretisation size at this level. The one at
%               the corresponding fine level is assumed to be dt/2^(lvl-1).
%    OMatch  - Integer. Defines up to which order a correction should be
%               applied in order to better approximate the Schur
%               complement. This should only be used if this problem is
%               solved in an MGRIT framework. If the number of coarse nodes
%               is just half wrt that of fine nodes, then I can improve
%               coarse correction by adding terms to the problem solved at
%               the coarser level, in the hope to better approximate the
%               Schur complement, at the price of additional cost.
%    lvl     - Integer. Defines the level at which this discretisation
%               needs to be used. Basically this defines how many steps of
%               the actual fine solver I need to approximate with the
%               operator defined here. The formula goes: steps = 2^(lvl-1).
%
%		Output:
%    f       - Matrix. Evaluations of f(u,t).
%    df      - (Possibly 3D) Matrix. Evaluations of jacobian of f, df(u,t).
%               UNSUPPORTED.
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% February 2019; Last revision: Feb-2019
%
%
%

if ( nargout > 1 )
	error('Burgers does not provide the Jacobian of the rhs (yet)')
end


if (nargin<5 || isempty(OMatch))
	OMatchL=-1;	% no correction applied by default
end

% in case you want some fancy initial conditions for testing:
% x = (dx/2):dx:(L-dx/2);			% these are actually the cell *centers*
% u0 =   ( x <= L/2 )*1 + ( x > L/2 )*0;


% recover some relevant quantities
NX  = size(u,1);		% number of cells in domain
dx = L/NX;					% cell size
NT = 2^(lvl-1);     % actual time steps at finest level

% rearrange variables adding ghost cells
% u =  [ u(1,:) ; u ; u(NX,:) ];				% Neumann BC: don't flip sign
% u =  [ u(1,:) ; u ; u(NX,:) ];			% Dirichlet BC: flip sign
% indeces for nodes left and right (taking periodic bc into account)
ip1 = circshift(1:NX,-1);
im1 = circshift(1:NX, 1);


% compute analytical flux at each cell
F  = u.*u/2;
% compute other useful variables:
DF = ( F( ip1,: ) - F( im1,: ) ) / (2*dx);  % centered derivative of flux
E  = ( u( ip1,: ) + u( im1,: ) ) /  2    ;  % average of sol over neighbouring cells

% by default, the rhs is given by (here I'm cheating, assuming FE time discretisation)
f = -u/dt  +  E/dt - DF;


% Then I can start correcting this guy, up to n-th order
if OMatch>=0 && lvl>1
	% - first of all, remove the current zeroth order approximation
	f = f - E/dt ;
	% - compute the part of the Schur complement that's accurate at zeroth order
	%    this is just given by E^n (u), where n is the number of steps at fine level
	En = E;
	for i=2:NT
		En = ( En(ip1,:) + En(im1,:) ) / 2;
	end
	% - finally, add correction to rhs
	f = f + En/dt;
end


if OMatch>=1 && lvl>1
	% - first of all, remove the current first order approximation
	f = f + DF;
	% - compute the part of the Schur complement that's accurate at first order
	%    this is given by - dt_F * ( E^{n}DF(u) + E^{n-1}DF(E(u)) + ... + ED(FE^{n-1}(u)) + DF(E^n(u)) )
	% this will contain, along the third dimension, all the terms in the
	% above sum
	EmDFEnu = u .* permute(ones(1,NT),[1,3,2]);
	
	for i=1:NT-1
		% hopefully this will work: at each iteration, at the right point, I am
		% - computing flux of En(u)
		% - computing its derivative
		% - substituting it at the right spot in EmDFEn(u)
		% - averaging again, to compute either En(u) or Em(DFEn(u))
		% and this is all nice and compact: ain't that great? hopefully it
		% works!
		FEm            =   EmDFEnu(  :,:,i)   .* EmDFEnu(  :,:,i)     /  2;
		EmDFEnu(:,:,i) = (     FEm(ip1,:  )   -      FEm(im1,:  ) )   / (2*dx);
		EmDFEnu        = ( EmDFEnu(ip1,:,:)   +  EmDFEnu(im1,:,:) )   /  2;
	end
	FEm              =   EmDFEnu(  :,:,end) .* EmDFEnu(  :,:,end)   /  2;
	EmDFEnu(:,:,end) = (     FEm(ip1,:    ) -      FEm(im1,:    ) ) / (2*dx);
	
	% - finally, add correction to rhs
	f = f - ( sum(EmDFEnu,3) ) / 2^(lvl-1);
end


% if OMatch>=2 && lvl>1
% 	% second order is actually dt^2 D( E DF )/4 (remember dt is taken at fine lvl, which is why it's halved)
% 	EDF = E.*DF;
% 	f = f + dt/4 * ( EDF(ip1,:) - EDF(im1,:) )/(2*dx);
% end
% 
% 
% if OMatch>=3 && lvl>1
% 	% third order is actually - dt^3 D( DF^2 )/16 (remember dt is taken at fine lvl, which is why it's halved)
% 	DF2 = DF.*DF;
% 	f = f - dt*dt/16 * ( DF2(ip1,:) - DF2(im1,:) )/(2*dx);
% end


if OMatch == -2						% just for debugging purposes: this sets the solution to the exact schur complement
	up1  = E - dt/NT*DF;
	
	for i = 2:NT
		% compute other useful variables:
		DFp1 = ( up1( ip1,: ).^2 - up1( im1,: ).^2 ) / (4*dx);  % centered derivative of flux
		Ep1  = ( up1( ip1,: )    + up1( im1,: )    ) /  2    ;  % average of sol over neighbouring cells
		% step
		up1 = Ep1 - dt/NT*DFp1;
	
	end

	f   = -u/dt + up1/dt;
end



end