function [f, df] = euler1D( u, ~, L, k, fType, g )
%euler1D right-hand side of system of 1D compressible Euler equations.
% 
% The 1D compressible Euler equations can be written (in conservative form) as:
%  dt(r)   = - dx( m )
%  dt(m)   = - dx( m^2/r + p )
%  dt(E)   = - dx( (E + p) *m/r )
% Here, r is density, m is momentum (=r*u) and E (=r*e) is total energy.
% Pressure is given by the formula p = r*ei*(g-1), where g=cp/cv and ei is 
% the specific internal energy, E = 0.5*m*m/r + r*ei.
% The domain is considered to be an interval of length L, where grid points
% are distributed uniformly. Neumann boundary conditions are applied (but
% other BC can be used by properly padding with ghost cells). Either
% Lax-Friedrichs OR Godunov flux (with Roe Riemann solver and a LeVeque
% entropic fix applied) can be prescribed. 
%
%
%
% Syntax:  [f, df] = euler1D( u, ~, L, k, fType, g )
%
%   Input:
%    u       - Matrix. Each column contains the state of the system at an
%               instant, specified in t, where the right-hand side needs to
%               be evaluated. The variables are density, r, momentum, m,
%               and energy, E: each column is hence split in three parts of
%               equal length.
%    t       - Vector. Contains time instants at which the right-hand side
%               needs to be evaluated (unused).
%    L       - Scalar. Size of domain. (Optional, default: 1).
%    k       - Integer. Order of WENO polynomial reconstruction. (Optional,
%               default: 0, corresponding to no reconstruction).
%    fType   - String. Type of flux employed in the scheme. Accepts two
%               values: LF for Lax-Friedrichs flux or ROE for Godunov flux
%               based on a Roe Riemann solver. (Optional, default: LF).
%    g       - Scalar. Ratio cp/cv. (Optional, default: 5/3, corresponding 
%               to an ideal monoatomic gas).
%
%		Output:
%    f       - Matrix. Evaluations of f(u,t).
%    df      - (Possibly 3D) Matrix. Evaluations of jacobian of f, df(u,t).
%               UNSUPPORTED.
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% January 2019; Last revision: May-2019
%
%
%

if ( nargout > 1 )
	error('Euler1D does not provide the Jacobian of the rhs (yet). It would be a bit of a pain to compute, after all...')
end

if (nargin<3 || isempty(L))
	L=1;
end

if (nargin<4 || isempty(k))
	k = 0;  % consider no reconstruction
end

if (nargin<5 || isempty(fType))
	fType = 'LF';  % consider no reconstruction
end

if (nargin<6 || isempty(g))
	g = 5/3;  % consider ideal, monoatomic gase
end


% in case you want some fancy initial conditions for testing (Sod's tube):
% x = (dx/2):dx:(L-dx/2);			% these are actually the cell *centers*
% r0 =   ( x <= L/2 )*1 + ( x > L/2 )*0.125;
% E0 = ( ( x <= L/2 )*1 + ( x > L/2 )*0.1 ) /(g-1);
% u0 = [ r0'; zeros(length(x),1); E0' ];



% recover some relevant quantities
N  = size(u,1)/3;		% number of cells in domain
dx = L/N;						% cell size


% Rearrange variables neatly: x-row,state variable-col, time-3rd dir
U = [ permute( u(     1:  N,: ), [3,1,2] ); ...			% r
			permute( u(   N+1:2*N,: ), [3,1,2] ); ...			% m
      permute( u( 2*N+1:end,: ), [3,1,2] ) ];				% E

		
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add ghost cells to include BC:
% % - neumann
% U = [ flip( U(:,1:k+1,:) ,2) , U , flip( U(:,(end-k):end,:),2) ];
% % - dirichlet
% U = [ flip( U(:,1:k+1,:) ,2) , U , flip( U(:,(end-k):end,:),2) ];	TO_TEST!!
% U(2,1:k+1,:) = -U(2,1:k+1,:); U(2,(end-k):end,:) = - U(2,(end-k):end,:); % flip velocity
% - periodic
U = [   U(:,(end-k):end,:) ,  U  ,  U(:,1:k+1,:) ];




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform characteristic-wise WENO reconstruction for each interface
% - values for reconstruction must be mapped to characteristic variables
% - this is done by picking the Roe average between state left and right of
%    the interface as a reference UC: variables on the other cells are
%    decomposed before reconstruction using eigs and eigv computed from UC.

% - compute some quantities of interest (trim extra ghost cells, keeping only those closest to domain)
r = U(1,(k+1):(end-k),:);		      % density
m = U(2,(k+1):(end-k),:);		      % momentum
E = U(3,(k+1):(end-k),:);		      % energy
v = m ./ r;		                    % velocity
v( r==0 ) = 0;										% avoid division by 0
p = (g-1) * ( E - 0.5*r.*v.*v );	% pressure
H = (E+p)./r;						          % specific total enthalpy
H( r==0 ) = 0;						        % avoid division by 0


% % Reconstruction based on interface value
% c = sqrt( g*p./r ) ;										% speed of sound
% % - compute (temporary) Roe averages at interface
% temp = sqrt(r);
% uBar = ( v(:,1:end-1,:).*temp(:,1:end-1,:) + v(:,2:end,:).*temp(:,2:end,:) ) ...
% 		 ./(                 temp(:,1:end-1,:) +               temp(:,2:end,:) );		% average velocity
% HBar = ( H(:,1:end-1,:)./temp(:,1:end-1,:) + H(:,2:end,:)./temp(:,2:end,:) ) ...
% 		 ./(                 temp(:,1:end-1,:) +               temp(:,2:end,:) );		% average enthalpy
% cBar = sqrt( (g-1)*( HBar - 0.5*uBar.*uBar ) );																	% average speed of sound
% l1Bar = uBar-cBar;																															% eigenvalues
% l2Bar = uBar;
% l3Bar = uBar+cBar;
% 
% % - initialise reconstructions
% UL = zeros(size(U,1), N+1, size(U,3));
% UR = zeros(size(U,1), N+1, size(U,3));
% for i = 1:N+1
% 	Uloc = U( :, i:(i+2*(k+1)-1), : );	% range of values used by WENO
% 	UlocChar = Uloc;
% % 	UlocChar = zeros(size(Uloc));			  % initialise characteristic values
% 	% decomposition is done by inverting the eigv matrix R computed at the
% 	% "interface" value (i). This gives (see "compute shock strengths" later)
% 
% % 	UlocChar(2,:,:) = (g-1)*( Uloc(1,:,:).*(HBar(:,i,:)-uBar(:,i,:).*uBar(:,i,:)) + uBar(:,i,:).*Uloc(2,:,:) - Uloc(3,:,:) )./(cBar(:,i,:).*cBar(:,i,:));
% % 	UlocChar(1,:,:) = ( ( Uloc(1,:,:).*l3Bar(:,i,:) - Uloc(2,:,:) )./cBar(:,i,:) - UlocChar(2,:,:) )/2;
% % 	UlocChar(3,:,:) = Uloc(1,:,:) - UlocChar(1,:,:) - UlocChar(2,:,:);
% 
% %   alpha1 = (g-1)*uBar(:,i,:).*uBar(:,i,:)./(2*cBar(:,i,:).*cBar(:,i,:));
% %   alpha2 = (g-1)./(cBar(:,i,:).*cBar(:,i,:));
% %   Rinv = [ ( alpha1 + uBar(:,i,:)./cBar(:,i,:))/2, -0.5*(alpha2*uBar(:,i,:) + 1./cBar(:,i,:)),  alpha2/2 ;...
% %            1-alpha1,                                                       alpha2*uBar(:,i,:), -alpha2   ;...
% %            ( alpha1 - uBar(:,i,:)./cBar(:,i,:))/2, -0.5*(alpha2*uBar(:,i,:) - 1./cBar(:,i,:)),  alpha2/2  ];
% % 	UlocChar = Rinv * Uloc;
% 	
% 	% now we can perform (local) WENO reconstruction:
% 	[ UL(:,i,:), UR(:,i,:) ] = wenoLocal( UlocChar, k );
% end
% % - map back to conserved variables
% rL = UL(1,:,:) + UL(2,:,:) + UL(3,:,:);
% rR = UR(1,:,:) + UR(2,:,:) + UR(3,:,:);
% mL = UL(1,:,:).*l1Bar + UL(2,:,:).*l2Bar + UL(3,:,:).*l3Bar;
% mR = UR(1,:,:).*l1Bar + UR(2,:,:).*l2Bar + UR(3,:,:).*l3Bar;
% EL = UL(1,:,:).*( HBar - uBar.*cBar ) + UL(2,:,:).*( 0.5* uBar.*uBar ) + UL(3,:,:).*( HBar + uBar.*cBar );
% ER = UR(1,:,:).*( HBar - uBar.*cBar ) + UR(2,:,:).*( 0.5* uBar.*uBar ) + UR(3,:,:).*( HBar + uBar.*cBar );


% Reconstruction based on cell value
c = sqrt( g*p./r );								% speed of sound
c( r==0 ) = 0;										% avoid division by 0
l1= v-c;													% eigenvalues
l2= v;
l3= v+c;

% - initialise reconstructions
UL = zeros(size(U,1), N+2, size(U,3));
UR = zeros(size(U,1), N+2, size(U,3));
for i = 1:N+2
	Uloc = U( :, i:(i+2*k), : );	% range of values used by WENO
	UlocChar = zeros(size(Uloc));			  % initialise characteristic values
  % decomposition is done by inverting the eigv matrix R computed at the
  % central cell value (i) (see "compute shock strengths" later)

	UlocChar(2,:,:) = (g-1)*( Uloc(1,:,:).*(H(:,i,:)-v(:,i,:).^2) + v(:,i,:).*Uloc(2,:,:) - Uloc(3,:,:) )./(c(:,i,:).^2);
	UlocChar(2, Uloc(1,:,:)==0 ) = 0;								% avoid division by 0
	UlocChar(1,:,:) = ( ( Uloc(1,:,:).*l3(:,i,:) - Uloc(2,:,:) )./c(:,i,:) - UlocChar(2,:,:) )/2;
	UlocChar(1, Uloc(1,:,:)==0 ) = 0;								% avoid division by 0
	UlocChar(3,:,:) = Uloc(1,:,:) - UlocChar(1,:,:) - UlocChar(2,:,:);
	
	% now we can perform (local) WENO reconstruction:
	[ UL(:,i,:), UR(:,i,:) ] = wenoLocalCell( UlocChar, k );
end
% - map back to conserved variables (before, R and L stood for left and
%    right interface of a given cell, now they stand for left and right
%    values at an interface)
rR = UL(1,2:end  ,:) + UL(2,2:end  ,:) + UL(3,2:end  ,:);
rL = UR(1,1:end-1,:) + UR(2,1:end-1,:) + UR(3,1:end-1,:);
mR = UL(1,2:end  ,:).* l1(:,2:end  ,:) + UL(2,2:end  ,:).*l2(:,2:end  ,:)   + UL(3,2:end  ,:).*l3(:,2:end  ,:);
mL = UR(1,1:end-1,:).* l1(:,1:end-1,:) + UR(2,1:end-1,:).*l2(:,1:end-1,:)   + UR(3,1:end-1,:).*l3(:,1:end-1,:);
ER = UL(1,2:end  ,:).*( H(:,2:end  ,:) - v(:,2:end  ,:) .* c(:,2:end  ,:) ) + UL(2,2:end  ,:).*( 0.5* v(:,2:end  ,:).^2 ) + UL(3,2:end  ,:).*( H(:,2:end  ,:) + v(:,2:end  ,:).*c(:,2:end  ,:) );
EL = UR(1,1:end-1,:).*( H(:,1:end-1,:) - v(:,1:end-1,:) .* c(:,1:end-1,:) ) + UR(2,1:end-1,:).*( 0.5* v(:,1:end-1,:).^2 ) + UR(3,1:end-1,:).*( H(:,1:end-1,:) + v(:,1:end-1,:).*c(:,1:end-1,:) );



% - recover additional handy variables
vL = mL./rL;
vL( rL==0 ) = 0;										% avoid division by 0
vR = mR./rR;
vR( rR==0 ) = 0;										% avoid division by 0
pL = (g-1) * ( EL - 0.5*rL.*vL.*vL );
pR = (g-1) * ( ER - 0.5*rR.*vR.*vR );
HL = (EL+pL)./rL;
HL( rL==0 ) = 0;										% avoid division by 0
HR = (ER+pR)./rR;
HR( rR==0 ) = 0;										% avoid division by 0
cL = sqrt( (g-1)*( HL - 0.5*vL.*vL ) );
cR = sqrt( (g-1)*( HR - 0.5*vR.*vR ) );




switch fType
	case 'LF'
		%% Lax-Friedrichs flux splitting
		% Lax-Friedrichs flux at interface is given by
		% F = ( F(uL)+F(uR) + q(uL-uR) ) / 2
		% with q = max(abs(eig(df/du)))
		
		% find maximum absolute value of eig for both left/right states
		maxlL = vL.*sign(vL) + cL;
		maxlR = vR.*sign(vR) + cR;
		q     = max( [ maxlL, maxlR ] );
		
		% compute fluxes left and right of interface
		FL = [ mL;                   ...
			     vL.*vL.*rL + pL;      ...
					 ( EL + pL ) .* vL ];
		FR = [ mR;                   ...
			     vR.*vR.*rR + pR;      ...
					 ( ER + pR ) .* vR ];
		
		% compute total flux through interface
		F = ( FL+FR + q * ( [ rL; mL; EL ] - [ rR; mR; ER ] ) ) / 2; 

		
	
	case 'ROE'
		%% Roe Riemann solver
		% The Roe scheme works by solving a Riemann problem at the cells interface.
		% Rather than solving the actual Non-linear Riemann problem arising from
		% the set of Euler equations, though, this problem is linearised around an
		% approriate averaged state. This is called Roe average, and the relevant
		% average quantities are defined as:
		% - v_avg = ( sqrt(rL)*vL + sqrt(rR)*vR ) / ( sqrt(rL) + sqrt(rR) )
		% - h_avg = ( sqrt(rL)*hL + sqrt(rR)*hR ) / ( sqrt(hL) + sqrt(hR) )

		% compute Roe averages at interface
		tempL = sqrt(rL);
		tempR = sqrt(rR);
		uBar = ( vL.*tempL + vR.*tempR ) ...
				 ./(     tempL +     tempR );		            % average velocity
		uBar( (tempL+tempR)==0 ) = 0;				            % avoid division by 0
		HBar = ( HL./tempL + HR./tempR ) ...
				 ./(     tempL +     tempR );		            % average enthalpy
		HBar( (tempL+tempR)==0 ) = 0;				            % avoid division by 0
		cBar = sqrt( (g-1)*( HBar - 0.5*uBar.*uBar ) );	% average speed of sound


		% Once the average state is provided, the problem solved is actually:
		% ut + Aux = 0, with A = DF|u=u_avg
		% The solution to such a linear problem is very easy to recover: the
		% eigenvalues of A identify 3 contact discontinuities, which split the
		% space-time domain in 4 different zones. The solution in any of these
		% zones can be recovered as u= uL + sum(lambda_p<x/t) alpha_p*r_p, that is,
		% every time we cross one such contact discontinuity, the solution jumps by
		% a value proportional to the corresponding eigenvactor r.

		% - Compute averaged eigenvalues and eigenvectors of A
		%  - for euler system, eigenvalues are given by: u-c, u, and u+c
		l1Bar = uBar-cBar;
		l2Bar = uBar;
		l3Bar = uBar+cBar;
		%  - for euler system, eigenvectors are given by:
		%   r1 = [1; u-c; H - u*c ]
		%   r2 = [1; u  ; 0.5*u*u ]
		%   r3 = [1; u+c; H + u*c ]
		r1Bar = [ones(size(l1Bar)); l1Bar; HBar - uBar.*cBar];
		r2Bar = [ones(size(l2Bar)); l2Bar;   0.5* uBar.*uBar];
		r3Bar = [ones(size(l3Bar)); l3Bar; HBar + uBar.*cBar];

		% - Decompose jump as linear combination of eigenvectors
		%  - compute jumps across interface uR-uL
		delta = [ rR - rL; ...
							mR - mL; ...
							ER - EL ];
		%  - compute shock strenghts alpha ( solving [r1|r2|r3] a = uR-uL )
		%   - invert [r1|r2|r3]:
		% temp2 = (g-1)./( cBar.^2 );
		% temp1 = temp2 .* uBar.^2 /2;
		% r1BarInv = [ ( temp1 + uBar./cBar ) /2    ;      1-temp1 ;  ( temp1 - uBar./cBar ) /2];
		% r2BarInv = [ -( temp2.*uBar + 1./cBar )/2 ;  uBar.*temp2 ; -( temp2.*uBar - 1./cBar )/2];
		% r3BarInv = [                     temp2 /2 ;       -temp2 ;                     temp2/2 ];
		% A = r1BarInv.*delta(1,:,:) + r2BarInv.*delta(2,:,:) + r3BarInv.*delta(3,:,:);
		% a1 = A(1,:,:);
		% a2 = A(2,:,:);
		% a3 = A(3,:,:);
		%   - or just use the right formula:
		a2 = (g-1)*( delta(1,:,:).*(HBar-uBar.*uBar) + uBar.*delta(2,:,:) - delta(3,:,:) )./(cBar.*cBar);
		a2( cBar == 0 ) = 0;				            % avoid division by 0
		a1 = ( ( delta(1,:,:).*l3Bar - delta(2,:,:) )./cBar - a2 )/2;
		a1( cBar == 0 ) = 0;				            % avoid division by 0
		a3 = delta(1,:,:) - a1 - a2;


		% Once this decomposition is available, the solution uI along the line x=0,
		% (ie the sonic line, which is the interface between the cells), can be
		% recovered equivalently as:
		%  uI = uL + sum(lambda_p< 0) alpha_p * r_p
		%  uI = uR - sum(lambda_p>=0) alpha_p * r_p
		% The Godunov flux is equivalently defined as
		%  FI = f(uL) + g(uI) - g(uL) = f(uL) + sum(lambda_p< 0) alpha_p * lambda_p * r_p
		%  FI = f(uR) + g(uI) - g(uR) = f(uR) - sum(lambda_p>=0) alpha_p * lambda_p * r_p
		% where g(u)=A*u is the flux for the approximate riemann solver. In this
		% formula we used the splitting above, the fact that A is linear, and the
		% definition of eigenvalues/eigenvector. Averaging these two expressions:
		%  FI = ( f(uL)+f(uR) )/2  - ( sum(p)  |lambda_p|* alpha_p *r_p ) /2

		% The problem with linear approximate Riemann solver is that they only
		% consider contact discontinuities (they're linear after all). While this
		% is fine most time, it becomes a bit weird when a transonic rarefaction
		% fan appears in the original problem. In this case, the state within the
		% fan (in particular, that at the centerline) needs to be reconstructed
		% with more care. To this purpose, a so-called "entropic fix" is applied.
		% A list of possible entropic fixes is given in:
		% "A review of entropy fixes as applied to Roe's linearization",
		% by Pelanti, M. and Quartapelle, L. and Vigevano, L..
		% From that, we picked the "4.3: Second entropy fix of Harten and Hyman,"
		% which is easy in the sense that it does not require computing eigenvalues
		% of intermediate states.
		% The fix substitutes the |lambda_p| in the flux formula with a function
		% q(lambda_p), defined as follows:

		% - Apply entropic fix
		% %  - compute actual eigenvalues for states at each cell
		l1L = vL-cL;
		l1R = vR-cR;
		l3L = vL+cL;
		l3R = vR+cR;
		%  - compare them with the average eigenval using:
		%    d = max( 0, lBar-l(uL), l(uR)-lBar )
		d1 = max(max( 0, l1Bar-l1L ), l1R-l1Bar );
		d3 = max(max( 0, l3Bar-l3L ), l3R-l3Bar );	% no need for d2 as that's never a rarefaction
		%  - substitute |lambda_p| with q(|lambda_p|)
		absL1Bar = abs( l1Bar );
		absL2Bar = abs( l2Bar );
		absL3Bar = abs( l3Bar );
		d1( d1 == 0 ) = absL1Bar( d1 == 0 ) - 1;	% these are just to avoid dividing by 0...
		d3( d3 == 0 ) = absL3Bar( d3 == 0 ) - 1;
		q1 = absL1Bar.*( absL1Bar >= d1 ) + 0.5 * ( l1Bar.*l1Bar./d1 + d1 ).*( absL1Bar < d1 );
		q2 = absL2Bar;	% no entropic fix for second eigenvalue
		q3 = absL3Bar.*( absL3Bar >= d3 ) + 0.5 * ( l3Bar.*l3Bar./d3 + d3 ).*( absL3Bar < d3 );

		% % Or just don't apply any entropic fix at all
% 		q1 = abs( l1Bar );
% 		q2 = abs( l2Bar );
% 		q3 = abs( l3Bar );


		% - Compute fluxes
		%  - fluxes at cell centers
		FL = [ mL;                   ...
			     vL.*vL.*rL + pL;      ...
					 ( EL + pL ) .* vL ];
		FR = [ mR;                   ...
			     vR.*vR.*rR + pR;      ...
					 ( ER + pR ) .* vR ];

		%  - Godunov flux at interface
		F = ( FL + FR ) /2 - ( q1.*a1.*r1Bar + ...
													 q2.*a2.*r2Bar + ...
													 q3.*a3.*r3Bar  )/2;

																					 
	otherwise
		error('Burger1D: Flux type prescribed not recognised. Only valid tags are LF or ROE')
end																					 
		
																					 
																					 
%  - either case, the total flux within the cell is given by
F = F(:,1:end-1,:) - F(:,2:end,:);

% Finally, reshape updates
f = permute(F,[2,3,1]);
f = [ f(:,:,1); ...
		  f(:,:,2); ...
		  f(:,:,3) ] / dx;



end