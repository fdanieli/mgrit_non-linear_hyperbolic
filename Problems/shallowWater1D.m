function [f, df] = shallowWater1D( u, ~, L, k, fType, g )
%shallowWater1D right-hand side of system of 1D Shallow-water equations.
% 
% The 1D Shallow-water equations can be written (in conservative form) as:
%  dt(h)  = - dx( m )
%  dt(m)  = - dx( m^2/h + 1/2*g*h^2 )
% Here, h is height, and m is momentum (=h*u). g is the gravitational
% constant.
% The domain is considered to be an interval of length L, where grid points
% are distributed uniformly. Neumann boundary conditions are applied (but
% other BC can be used by properly padding with ghost cells). Either
% Lax-Friedrichs OR Godunov flux (with Roe Riemann solver and a LeVeque
% entropic fix applied) can be prescribed. 
%
%
% Syntax:  [f, df] = shallowWater1D( u, ~, L, k, fType, g )
%
%   Input:
%    u       - Matrix. Each column contains the state of the system at an
%               instant, specified in t, where the right-hand side needs to
%               be evaluated. The variables are height, h, and momentum, m:
%               each column is hence split in two parts of equal length.
%    t       - Vector. Contains time instants at which the right-hand side
%               needs to be evaluated (unused).
%    L       - Scalar. Size of domain. (Optional, default: 1).
%    k       - Integer. Order of WENO polynomial reconstruction. (Optional,
%               default: 0, corresponding to no reconstruction).
%    fType   - String. Type of flux employed in the scheme. Accepts two
%               values: LF for Lax-Friedrichs flux or ROE for Godunov flux
%               based on a Roe Riemann solver. (Optional, default: LF).
%    g       - Scalar. Gravitational constant. (Optional, default: 9.8).
%
%		Output:
%    f       - Matrix. Evaluations of f(u,t).
%    df      - (Possibly 3D) Matrix. Evaluations of jacobian of f, df(u,t).
%               UNSUPPORTED.
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% June 2019; Last revision: Jun-2019
%
%
%

if ( nargout > 1 )
	error('shallowWater1D does not provide the Jacobian of the rhs (yet). It would be a bit of a pain to compute, after all...')
end

if (nargin<3 || isempty(L))
	L=1;
end

if (nargin<4 || isempty(k))
	k = 0;   % consider no reconstruction
end

if (nargin<5 || isempty(fType))
	fType = 'LF';
end

if (nargin<6 || isempty(g))
	g = 9.8;
end




% Recover some relevant quantities
N  = size(u,1)/2;		% number of cells in domain
dx = L/N;						% cell size

% Rearrange variables neatly: x-row,state variable-col, time-3rd dir
U = [ permute( u(   1:  N,: ), [3,1,2] ); ...			% h
      permute( u( N+1:2*N,: ), [3,1,2] ) ];				% m

		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add ghost cells to include BC:
% % - neumann
% U = [ flip( U(:,1:k+1,:) ,2) , U , flip( U(:,(end-k):end,:),2) ];
% % - dirichlet
% U = [ flip( U(:,1:k+1,:) ,2) , U , flip( U(:,(end-k):end,:),2) ];	TO_TEST!!
% U(2,1:k+1,:) = -U(2,1:k+1,:); U(2,(end-k):end,:) = - U(2,(end-k):end,:);
% - periodic
U = [   U(:,(end-k):end,:) ,  U  ,  U(:,1:k+1,:) ];




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform characteristic-wise WENO reconstruction for each interface
% - values for reconstruction must be mapped to characteristic variables
% - this is done by picking the Roe average between state left and right of
%    the interface as a reference UC: variables on the other cells are
%    decomposed before reconstruction using eigs and eigv computed from UC.

% - compute some quantities of interest (trim extra ghost cells, keeping only those closest to domain)
h = U(1,(k+1):(end-k),:);		          % height
m = U(2,(k+1):(end-k),:);		          % momentum
v = m ./ h;		                        % velocity
v( h==0 ) = 0;												% avoid division by 0
c = sqrt( g*h );                   		% speed of sound

% Reconstruction based on interface value
% % - compute (temporary) Roe average
% hBar = ( h(:,1:end-1,:) + h(:,2:end,:) ) / 2;                             % average height
% uBar = ( v(:,1:end-1,:).*c(:,1:end-1,:) + v(:,2:end,:).*c(:,2:end,:) ) ...
% 		 ./(                 c(:,1:end-1,:) +               c(:,2:end,:) );		% average velocity (we recycled c = sqrt(h))
% cBar = sqrt( g*hBar );		                                                % average speed of sound
% l1Bar= uBar - cBar;									                                      % eigenvalues
% l2Bar= uBar + cBar;
% % - initialise reconstructions
% UL = zeros(size(U,1), N+1, size(U,3));
% UR = zeros(size(U,1), N+1, size(U,3));
% for i = 1:N+1
% 	Uloc = U( :, i:(i+2*(k+1)-1), : );	% range of values used by WENO
% 	UlocChar = zeros(size(Uloc));			  % initialise characteristic values
% 	% decomposition is done by inverting the eigv matrix R computed at the
% 	% "interface" value (i). This gives (see "compute shock strengths" below)
% 	UlocChar(1,:,:) = ( Uloc(1,:,:).*l2Bar(1,i,:) - Uloc(2,:,:) )./( 2*cBar(1,i,:) );
% 	UlocChar(2,:,:) = Uloc(1,:,:) - UlocChar(1,:,:);
% 	% now we can perform (local) WENO reconstruction:
% 	[ UL(:,i,:), UR(:,i,:) ] = wenoLocal( UlocChar, k );
% end
% % - map back to conserved variables (see definition of eigv below)
% hL = UL(1,:,:) + UL(2,:,:);
% hR = UR(1,:,:) + UR(2,:,:);
% mL = ( UL(1,:,:).*l1Bar + UL(2,:,:).*l2Bar );
% mR = ( UR(1,:,:).*l1Bar + UR(2,:,:).*l2Bar );



% Reconstruction based on cell value
l1= v-c;													% eigenvalues
l2= v+c;

% - initialise reconstructions
UL = zeros(size(U,1), N+2, size(U,3));
UR = zeros(size(U,1), N+2, size(U,3));
for i = 1:N+2
	Uloc = U( :, i:(i+2*k), : );	% range of values used by WENO
	UlocChar = zeros(size(Uloc));			  % initialise characteristic values
% 	decomposition is done by inverting the eigv matrix R computed at the
% 	central cell value (i) (see "compute shock strengths" later)
	UlocChar(1,:,:) = ( Uloc(1,:,:).*l2(1,i,:) - Uloc(2,:,:) )./( 2*c(1,i,:) );
	UlocChar(1, Uloc(1,:,:)==0 ) = 0;								% avoid division by 0
	UlocChar(2,:,:) =  Uloc(1,:,:) - UlocChar(1,:,:);
	% now we can perform (local) WENO reconstruction:
	[ UL(:,i,:), UR(:,i,:) ] = wenoLocalCell( UlocChar, k );
end
% - map back to conserved variables (before, R and L stood for left and
%    right interface of a given cell, now they stand for left and right
%    values at an interface)
hR = UL(1,2:end  ,:) + UL(2,2:end  ,:);
hL = UR(1,1:end-1,:) + UR(2,1:end-1,:);
mR = UL(1,2:end  ,:).* l1(:,2:end  ,:) + UL(2,2:end  ,:).*l2(:,2:end  ,:);
mL = UR(1,1:end-1,:).* l1(:,1:end-1,:) + UR(2,1:end-1,:).*l2(:,1:end-1,:);


% - recover additional handy variables
vL = mL./hL;
vL( hL==0 ) = 0;												% avoid division by 0
vR = mR./hR;
vR( hR==0 ) = 0;												% avoid division by 0
cL = sqrt( g*hL );
cR = sqrt( g*hR );


switch fType
	case 'LF'
		%% Lax-Friedrichs flux splitting
		% Lax-Friedrichs flux at interface is given by
		% F = ( F(uL)+F(uR) + q(uL-uR) ) / 2
		% with q = max(abs(eig(df/du)))
		
		% find maximum absolute value of eig for both left/right states
		maxlL = vL.*sign(vL) + cL;
		maxlR = vR.*sign(vR) + cR;
		q     = max( [ maxlL, maxlR ] );
				 
		% compute fluxes left and right of interface
		FL = [ mL;                   ...
			     vL.*vL.*hL + 0.5*g*hL.*hL ];
		FR = [ mR;                   ...
			     vR.*vR.*hR + 0.5*g*hR.*hR ];
				 
		% compute total flux through interface
		F = ( FL+FR + q * ( [ hL; mL ] - [ hR; mR ] ) ) / 2; 

		
		case 'ROE'
		%% Roe Riemann solver
		% The Roe scheme works by solving a Riemann problem at the cells interface.
		% Rather than solving the actual Non-linear Riemann problem arising from
		% the set of Euler equations, though, this problem is linearised around an
		% approriate averaged state. This is called Roe average, and the relevant
		% average quantities are defined as:
		% - h_avg = ( hL + hR ) / 2
		% - v_avg = ( sqrt(hL)*vL + sqrt(hR)*vR ) / ( sqrt(hL) + sqrt(hR) )

		% compute Roe averages at interface
		hBar = ( hL + hR ) / 2;       % average height
		uBar = ( vL.*cL + vR.*cR ) ...
				 ./(     cL +     cR );		% average velocity (we recycled c = sqrt(gh))
		uBar( (hL+hR)==0 ) = 0;				% avoid division by 0
		cBar = sqrt( g*hBar ) ;		    % average speed of sound


		% Once the average state is provided, the problem solved is actually:
		% ut + Aux = 0, with A = DF|u=u_avg
		% The solution to such a linear problem is very easy to recover: the
		% eigenvalues of A identify 3 contact discontinuities, which split the
		% space-time domain in 4 different zones. The solution in any of these
		% zones can be recovered as u= uL + sum(lambda_p<x/t) alpha_p*r_p, that is,
		% every time we cross one such contact discontinuity, the solution jumps by
		% a value proportional to the corresponding eigenvactor r.

		% - Compute averaged eigenvalues and eigenvectors of A
		%  - for shallow-water system, eigenvalues are given by: u-c, and u+c
		l1Bar = uBar-cBar;
		l2Bar = uBar+cBar;
		%  - for shallow-water system, eigenvectors are given by:
		%   r1 = [1; u-c ]
		%   r2 = [1; u+c ]
		r1Bar = [ones(size(l1Bar)); l1Bar];
		r2Bar = [ones(size(l2Bar)); l2Bar];

		% - Decompose jump as linear combination of eigenvectors
		%  - compute jumps across interface uR-uL
		delta = [ hR - hL; ...
							mR - mL];
		%  - compute shock strenghts alpha ( solving [r1|r2] a = uR-uL )
		a1 = ( delta(1,:,:).*l2Bar - delta(2,:,:) )./( 2*cBar );
		a1( cBar==0 ) = 0;					% avoid division by 0
		a2 = delta(1,:,:) - a1;

		% Once this decomposition is available, the solution uI along the line x=0,
		% (ie the sonic line, which is the interface between the cells), can be
		% recovered equivalently as:
		%  uI = uL + sum(lambda_p< 0) alpha_p * r_p
		%  uI = uR - sum(lambda_p>=0) alpha_p * r_p
		% The Godunov flux is equivalently defined as
		%  FI = f(uL) + g(uI) - g(uL) = f(uL) + sum(lambda_p< 0) alpha_p * lambda_p * r_p
		%  FI = f(uR) + g(uI) - g(uR) = f(uR) - sum(lambda_p>=0) alpha_p * lambda_p * r_p
		% where g(u)=A*u is the flux for the approximate riemann solver. In this
		% formula we used the splitting above, the fact that A is linear, and the
		% definition of eigenvalues/eigenvector. Averaging these two expressions:
		%  FI = ( f(uL)+f(uR) )/2  - ( sum(p)  |lambda_p|* alpha_p *r_p ) /2

		% The problem with linear approximate Riemann solver is that they only
		% consider contact discontinuities (they're linear after all). While this
		% is fine most time, it becomes a bit weird when a transonic rarefaction
		% fan appears in the original problem. In this case, the state within the
		% fan (in particular, that at x=0) needs to be reconstructed with more
		% care. To this purpose, a so-called "entropic fix" is applied.
		% A list of possible entropic fixes is given in:
		% "A review of entropy fixes as applied to Roe's linearization",
		% by Pelanti, M. and Quartapelle, L. and Vigevano, L..
		% From that, we picked the "4.3: Second entropy fix of Harten and Hyman,"
		% which is easy in the sense that it does not require computing eigenvalues
		% of intermediate states.
		% The fix substitutes the |lambda_p| in the flux formula with a function
		% q(lambda_p), defined as follows:

		% - Apply entropic fix
		%  - get actual eigenvalues for states at each cell (including ghost cells closest to boundary)
		% %  - compute actual eigenvalues for states at each cell
		l1L = vL-cL;
		l1R = vR-cR;
		l2L = vL+cL;
		l2R = vR+cR;
		%  - compare them with the average eigenval using:
		%    d = max( 0, lBar-l(uL), l(uR)-lBar )
		d1 = max(max( 0, l1Bar-l1L ), l1R-l1Bar );
		d2 = max(max( 0, l2Bar-l2L ), l2R-l2Bar );	% no need for d2 as that's never a rarefaction
		%  - substitute |lambda_p| with q(|lambda_p|)
		absL1Bar = abs( l1Bar );
		absL2Bar = abs( l2Bar );
		d1( d1 == 0 ) = absL1Bar( d1 == 0 ) - 1;	% these are just to avoid dividing by 0...
		d2( d2 == 0 ) = absL2Bar( d2 == 0 ) - 1;
		q1 = absL1Bar.*( absL1Bar >= d1 ) + 0.5 * ( l1Bar.*l1Bar./d1 + d1 ).*( absL1Bar < d1 );
		q2 = absL2Bar.*( absL2Bar >= d2 ) + 0.5 * ( l2Bar.*l2Bar./d2 + d2 ).*( absL2Bar < d2 );
		% % Or just don't apply any entropic fix at all
		% q1 = abs( l1Bar );
		% q2 = abs( l2Bar );

		% - Compute fluxes left and right of interface
		FL = [ mL;                   ...
					 vL.*vL.*hL + 0.5*g*hL.*hL ];
		FR = [ mR;                   ...
					 vR.*vR.*hR + 0.5*g*hR.*hR ];


		%  - Godunov flux at interface
		F = ( FL + FR ) /2 - ( q1.*a1.*r1Bar + ...
													 q2.*a2.*r2Bar )/2;

										 
	otherwise
		error('Burger1D: Flux type prescribed not recognised. Only valid tags are LF or ROE')
end																					 
		
										 
%  - either case, the total flux within the cell is given by
F = F(:,1:end-1,:) - F(:,2:end,:);


% Finally, reshape updates
f = permute(F,[2,3,1]);
f = [ f(:,:,1); ...
		  f(:,:,2) ] / dx;



end