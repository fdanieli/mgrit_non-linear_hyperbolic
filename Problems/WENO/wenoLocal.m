function [ uL, uR ] = wenoLocal( u, k )
%wenoLocal WENO reconstruction of interface state/flux for conservation laws.
%
% This is a "local" version of the function weno. It assumes only one
% interface needs to be reconstructed (useful to avoid redundancy in
% characteristic-based reconstructions for systems of conservation laws).
% See weno for more info.
% 
%
% Syntax:  [ uL, uR ] = wenoLocal( u, k )
%
%   Input:
%    u       - Matrix. Each column contains the state of the system at a
%               specific cell in the domain. Each row is treated
%               independently, and represents the values of a state
%               variable along the domain. Here, we assume only 2(k+1)
%               columns are present, one per cell of the stencils.
%    k       - Scalar. The stencils picked have size k+1. It relates to the
%               order of scheme (=2k-1).
%
%		Output:
%    uL      - Vector. Reconstructed state left of the central interface
%               (see sketch below).
%    uR      - Vector. Reconstructed state right of the central interface
%               (see sketch below).
%                                         uL uR 
%               |<---i=1--->|...|<--i=k+1-->|<--i=k+2-->|...|<--i=end-->|
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% June 2019; Last revision: Jun-2019
%

%% Initialisation

% Bunch of useful parameters
myEps = 1e-6; % to prevent division by 0


% Initialise relevant quantities
% A few things need to be done to perform reconstruction:
% - Evaluate all possible k-th order interpolating polynomials. There are
%    k+2 polynomials to choose from, and they depend on how the stencil is
%    centered around the interface. For example, if we choose a 2nd order
%    reconstruction, we can position its corresponding 3-cell stencil in 4
%    different ways to recover an evaluation at the central interface:     
%    |--|--|->|<-|--|--|
%             |--------|			r =-1
%          |--------|					r = 0
%       |--------|            r = 1
%    |--------|               r = 2 = k+2
%    Evaluating polynomials reduces to linearly combine appropriately the
%    cell values using specific weights (see eq(2.21) / Table(2.1) in [1]) 
% - Rconstruct left/right values for interface at k+1.5 by performing a
%    convex combination of the polynomial evaluations previously recovered.
%    This boils down to accurately picking weights for each polynomial.
%    These depend on the quality of each polynomial approximation, which is
%    estimated using "smooth indicators" (eq(2.61) in [1])
%    Notice only those eveluations for which the cell *is in the stencil*
%    should be considered. This removes the very first polynomial
%    considered above (see definition of C below) for the L-values, and the
%    very last polynomial for the R-values.
%
% Next are defined necessary quantities to perform the reconstruction:
% C - weights for polynomials evaluations. Each row identifies different
%     polynomials, each column gives the necessary weights. The first
%     polynomial (row) has a stencil starting right of the considered
%     interface. Going down each row, we shift the stencil left by one.
% D - parameter for weights for convex combination of polynomial evals.
% B - values of bilinear form defining the smoothness indicators

% % TODO: WENO can be generalised to any order provided a routine to
% compute matrices B, C, and D is given. For now we just limit ourselves to
% order up to 3.


switch k
	case 0	% no reconstruction whatsoever
		C = [ 1 ;...
			    1 ];

		D = 1;

		B = 1;
		
		
	case 1
		C = [ 3, -1 ; ...
			    1,  1 ; ...
				 -1,  3	] / 2;

		D = [ 2;  1 ] / 3;
		
		B = [  1, -1   ,    1, -1 ; ...
			    -1,  1   ,   -1,  1  ];		
		B = reshape( B, [k+1,k+1,k+1] );
		
	case 2
		C = [ 11, -7,  2 ; ...
			     2,  5, -1 ; ...
				  -1,  5,  2 ; ...
					 2, -7, 11 ] / 6;
		
		D = [  3;  6;  1 ] / 10;
		
		B = [  20, -31,  11   ,    8, -13,   5   ,    8, -19,  11; ...
			    -31,  50, -19   ,  -13,  26, -13   ,  -19,  50, -31; ...
					 11, -19,   8   ,    5, -13,   8   ,   11, -31,  20 ] / 6;			
		B = reshape( B, [k+1,k+1,k+1] );
	
	case 3
		C = [ 25, -23,  13, -3; ...
				   3,  13,  -5,  1; ...
	        -1,   7,   7, -1; ...
        	 1,  -5,  13,  3; ...
	        -3,  13, -23, 25 ] / 12;
		
		
		D = [  4; 18; 12; 1 ] / 35;
		
		B = [  2107, -4701,  3521, - 927   ,    547, -1261,   961, -247   ,   267, - 821,   801, - 247   ,   547, -1941,   2321, - 927; ...
          -4701, 11003, -8623,  2321   ,  -1261,  3443, -2983,  801   ,  -821,  2843, -2983,   961   , -1941,  7043, - 8623,  3521; ...
           3521, -8623,  7043, -1941   ,    961, -2983,  2843, -821   ,   801, -2983,  3443, -1261   ,  2321, -8623,  11003, -4701; ...
          - 927,  2321, -1941,   547   ,  - 247,   801, - 821,  267   ,  -247,   961, -1261,   547   , - 927,  3521, - 4701,  2107; ] /240;
		B = reshape( B, [k+1,k+1,k+1] );

	otherwise
		error('weno: scheme of order higher than 3 not implemented yet')
end		





%% Reconstruction 
% compute values at interface for each polynomial
uC = zeros( size(u,1), k+2 );
for kk = 1:k+2
	uC( :, kk ) = u( :, (end-kk-k+1):(end-kk+1) ) * C( kk, : )';
end


% evaluate smoothness indicators
SIL = zeros( size(u,1), k+1 );
SIR = zeros( size(u,1), k+1 );
for kk=1:k+1
	for jj=1:size(u,1)
		SIL( jj, kk ) = u( jj, (end-kk-k  ):(end-kk  ) ) * B( :, :, kk ) * u( jj, (end-kk-k  ):(end-kk  ) ).';
		SIR( jj, kk ) = u( jj, (end-kk-k+1):(end-kk+1) ) * B( :, :, kk ) * u( jj, (end-kk-k+1):(end-kk+1) ).';
	end
end

% use smoothness indicators to compute weights
omL =      D'  ./ ( myEps + SIL ).^2;
omR = flip(D)' ./ ( myEps + SIR ).^2;
% normalise
omL = omL ./ sum(omL,2);
omR = omR ./ sum(omR,2);


% finally perform convex combination
uL = sum( uC( :, 2:end   ) .* omL, 2 );
uR = sum( uC( :, 1:end-1 ) .* omR, 2 );


% % for debugging
% u0=[-sin(0:0.1:pi), sin(0:0.1:pi)-1];
% k = 3;
% [uL,uR] = weno(u0,k);
% uL = uL( :, (k+2):(end-k-1) );
% uR = uR( :, (k+2):(end-k-1) );
% uL2 = zeros( size( uL ) );
% uR2 = zeros( size( uR ) );
% for i=1:size(uL2,2)
% 	[ uL2(:,i), uR2(:,i) ] = wenoLocal( u0( :, i:(i+2*k+1) ), k );
% end
% 
% max(max(abs(uL2-uL)))
% max(max(abs(uR2-uR)))


end