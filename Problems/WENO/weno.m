function [ uL, uR ] = weno( u, k )
%weno WENO reconstruction of interface states/fluxes for conservation laws
%
% The Weighted Essentially Non-Oscillatory (WENO) scheme uses a high-order
% reconstruction of quantities at the interfaces in order to attain
% high-accuracy while preventing oscillations in the solution.
% Details can be found in
% [1] "Essentially Non-Oscillatory and Weighted Essentially Non-Oscillatory
%      Schemes for Hyperbolic Conservation Laws", Chi-Wang Shu
%      (https://www3.nd.edu/~zxu2/acms60790S13/Shu-WENO-notes.pdf)
% [2] "Numerical Methods for Conservation Laws: From Analysis to
%      Algorithms", 2018, Jan S. Hesthaven
%      (https://epubs.siam.org/doi/book/10.1137/1.9781611975109?mobileUi=0)
%
% In brief, quantities of interest at the interface of a cell are
% reconstructed up to (2k-1)th order accuracy by using a convex combination
% of all interpolating polynomials which can be bilt using a stencil of
% size k containing the cell. One can choose to reconstruct the state (and
% recover the flux from that) or directly reconstruct the flux: the routine
% works in the same way in both cases (but we will just refer to "state"
% from now on).
%
% NOTE: Periodic boundary conditions are assumed by default. Other BC can
% however be computed by padding the values with ghost cells, and then
% trimming them again (this needs to be done outside this routine, though).
%
%
% Syntax:  [ uL, uR ] = weno( u, k )
%
%   Input:
%    u       - Matrix. Each column contains the state of the system at a
%               specific cell in the domain. Each row is treated
%               independently, and represents the values of a state
%               variable along the domain.
%    k       - Scalar. The stencils picked have size k+1. It relates to the
%               order of scheme (=2k-1).
%
%		Output:
%    uL      - Matrix. Reconstructed state left of each interface (see
%               sketch below).
%    uR      - Matrix. Reconstructed state right of each interface (see
%               sketch below).
%               uL(1) uR(1)   uL(2) uR(2)   uL(3) uR(3) ... uL(end) uR(end)
%                    |<----i=1---->|<----i=2---->|...|<---i=end--->|
%
% NOTE: Due to periodic BC, uL(:,end) = uL(:,1) uR(:,end) = uR(:,1). That
% is, last value is repeated for convenience.
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% April 2019; Last revision: May-2019
%

%% Initialisation

% Bunch of useful parameters
N     = size(u,2);
myEps = 1e-6; % to prevent division by 0


% Initialise relevant quantities
% A few things need to be done to perform reconstruction:
% - Evaluate all possible k-th order interpolating polynomials, at all
%    interfaces. There are k+2 polynomials to choose from, and they depend
%    on how the stencil is centered around the interface. For example, if
%    we choose a 2nd order reconstruction, we can position its
%    corresponding 3-cell stencil in 4 different ways to recover an
%    evaluation at the central interface:     
%    |--|--|->|<-|--|--|
%             |--------|			r =-1
%          |--------|					r = 0
%       |--------|            r = 1
%    |--------|               r = 2 = k+2
%    Evaluating polynomials reduces to linearly combine appropriately the
%    cell values using specific weights (see eq(2.21) / Table(2.1) in [1]) 
% - For each cell i, reconstruct values at i+/-0.5 by performing a convex
%    combination of the polynomial evaluations previously recovered. This
%    boils down to accurately picking weights for each polynomial. These
%    depend on the quality of each polynomial approximation, which is
%    estimated using "smooth indicators" (eq(2.61) in [1])
%    Notice only those eveluations for which the cell *is in the stencil*
%    should be considered. This removes the very first polynomial
%    considered above (see definition of C below) for the L-values, and the
%    very last polynomial for the R-values.
%
% Next are defined necessary quantities to perform the reconstruction:
% C - weights for polynomials evaluations. Each row identifies different
%     polynomials, each column gives the necessary weights. The first
%     polynomial (row) has a stencil starting right of the considered
%     interface. Going down each row, we shift the stencil left by one.
% D - parameter for weights for convex combination of polynomial evals.
% B - values of bilinear form defining the smoothness indicators

% % TODO: WENO can be generalised to any order provided a routine to
% compute matrices B, C, and D is given. For now we just limit ourselves to
% order up to 3.


switch k
	case 0	% no reconstruction whatsoever
		C = [ 1 ;...
			    1 ];

		D = 1;

		B = 1;

		pEval = @(u) reshape( [ circshift( u, [0,-1] ) ...
			                      u                      ], ...
													[size(u,1),size(u,2),2] );					
		beta = @(u) ones(size(u));	% dummy value
		
		
	case 1
		C = [ 3, -1 ; ...
			    1,  1 ; ...
				 -1,  3	] / 2;

		D = [ 2;  1 ] / 3;
		
		B = [  1, -1   ,    1, -1 ; ...
			    -1,  1   ,   -1,  1  ];		
		B = reshape( B, [k+1,k+1,k+1] );

		pEval = @(u) reshape( [( 3*circshift( u, [0,-1] ) -   circshift( u, [0,-2] )) ...
			                     (   u                      +   circshift( u, [0,-1] )) ...
													 ( - circshift( u, [0, 1] ) + 3*u                     ) ] /2, ...
													[size(u,1),size(u,2),3] );						
		
		beta = @(u) reshape( [ ( circshift( u, [0,-1] ) - u ).^2   ,...
			                     ( u - circshift( u, [0,1] )  ).^2 ] ,...
													 [size(u,1),size(u,2),2] );
		
		
	case 2
		C = [ 11, -7,  2 ; ...
			     2,  5, -1 ; ...
				  -1,  5,  2 ; ...
					 2, -7, 11 ] / 6;
		
		D = [  3;  6;  1 ] / 10;
		
		B = [  20, -31,  11   ,    8, -13,   5   ,    8, -19,  11; ...
			    -31,  50, -19   ,  -13,  26, -13   ,  -19,  50, -31; ...
					 11, -19,   8   ,    5, -13,   8   ,   11, -31,  20 ] / 6;			
		B = reshape( B, [k+1,k+1,k+1] );

		pEval = @(u) reshape( [(11*circshift( u, [0,-1] ) - 7*circshift( u, [0,-2] ) +  2*circshift( u, [0,-3] )) ...
			                     ( 2*u                      + 5*circshift( u, [0,-1] ) -    circshift( u, [0,-2] )) ...
			                     (-  circshift( u, [0, 1] ) + 5*u                      +  2*circshift( u, [0,-1] )) ...
													 ( 2*circshift( u, [0, 2] ) - 7*circshift( u, [0, 1] ) + 11*u                     ) ] /6, ...
													  [size(u,1),size(u,2),4] );						
		
		beta = @(u) reshape( [ 13/12 * ( u                      - 2*circshift( u, [0,-1] ) + circshift( u, [0,-2] ) ).^2 + ( 3*u                      - 4*circshift( u, [0,-1] ) + circshift( u, [0,-2] ) ).^2 /4 ...
													,13/12 * ( circshift( u, [0, 1] ) - 2*u                      + circshift( u, [0,-1] ) ).^2 + (   circshift( u, [0, 1] )                            - circshift( u, [0,-1] ) ).^2 /4 ...
													,13/12 * ( circshift( u, [0, 2] ) - 2*circshift( u, [0, 1] ) + u                      ).^2 + (   circshift( u, [0, 2] ) - 4*circshift( u, [0, 1] ) + 3*u                    ).^2 /4    ] , ...
													 [size(u,1),size(u,2),3] );		
	
	case 3
		C = [ 25, -23,  13, -3; ...
				   3,  13,  -5,  1; ...
	        -1,   7,   7, -1; ...
        	 1,  -5,  13,  3; ...
	        -3,  13, -23, 25 ] / 12;
		
		
		D = [  4; 18; 12; 1 ] / 35;
		
		B = [  2107, -4701,  3521, - 927   ,    547, -1261,   961, -247   ,   267, - 821,   801, - 247   ,   547, -1941,   2321, - 927; ...
          -4701, 11003, -8623,  2321   ,  -1261,  3443, -2983,  801   ,  -821,  2843, -2983,   961   , -1941,  7043, - 8623,  3521; ...
           3521, -8623,  7043, -1941   ,    961, -2983,  2843, -821   ,   801, -2983,  3443, -1261   ,  2321, -8623,  11003, -4701; ...
          - 927,  2321, -1941,   547   ,  - 247,   801, - 821,  267   ,  -247,   961, -1261,   547   , - 927,  3521, - 4701,  2107; ] /240;
		B = reshape( B, [k+1,k+1,k+1] );

% 		pEval = @(u) reshape( [(25*circshift( u, [0,-1] ) - 23*circshift( u, [0,-2] ) + 13*circshift( u, [0,-3] ) -  3*circshift( u, [0,-4] )) ...
% 			                     ( 3*u                      + 13*circshift( u, [0,-1] ) -  5*circshift( u, [0,-2] ) +    circshift( u, [0,-3] )) ...
% 			                     (-  circshift( u, [0, 1] ) +  7*u                      +  7*circshift( u, [0,-1] ) -    circshift( u, [0,-2] )) ...
% 													 (   circshift( u, [0, 2] ) -  5*circshift( u, [0, 1] ) + 13*u                      +  3*circshift( u, [0,-1] )) ...
% 													 (-3*circshift( u, [0, 3] ) + 13*circshift( u, [0, 2] ) - 23*circshift( u, [0, 1] ) + 25*u                     ) ] /12, ...
% 													  [size(u,1),size(u,2),5] );						

	otherwise
		error('weno: scheme of order higher than 3 not implemented yet')
end		





%% Reconstruction 
% compute values at interface for each polynomial
% uC = pEval(u);
uC = zeros( size(u,1), size(u,2), k+2 );
for kk = 1:k+2
	for ii = 1:k+1
		uC( :, :, kk ) = uC( :, :, kk ) + C( kk, ii )*circshift( u, [0,-ii+kk-1]);
	end
end


% evaluate smoothness indicators
% SI = beta(u);
SI = zeros( size(u,1),size(u,2), k+1 );
for kk=1:k+1
	for ii=1:k+1
		SI(:,:,kk) = SI(:,:,kk) + B(ii,ii,kk) * circshift( u, [0,-ii+kk] ) .* circshift( u, [0,-ii+kk] );
		for jj=ii+1:k+1
			SI(:,:,kk) = SI(:,:,kk) + 2* B(ii,jj,kk) * circshift( u, [0,-ii+kk] ) .* circshift( u, [0,-jj+kk] );
		end
	end
end


% use smoothness indicators to compute weights
omL = permute(      D , [3,2,1] ) ./ ( myEps + SI ).^2;
omR = permute( flip(D), [3,2,1] ) ./ ( myEps + SI ).^2;
% normalise
omL = omL ./ sum(omL,3);
omR = omR ./ sum(omR,3);


% finally perform convex combination
uL = sum( uC( :,                    :, 2:end   ).*omL, 3 );
uR = sum( uC( :, circshift(1:N,[0,1]), 1:end-1 ).*omR, 3 );
% (the circshift for uR is necessary, since the values considered are
% shifted by 1, in that case)


% As a final touch, we just need to adjust the order of the vectors so that
% values left and right of the interfaces are paired
uL = [ uL(:,end), uL ];
uR = [ uR, uR(:,  1) ];



% % for debugging
% u0=[-sin(0:0.1:pi), sin(0:0.1:pi)-1];
% x  = linspace(0,1,N);
% x2 = linspace(0-1/(2*N),1+1/(2*N),N+1);
% plot(x , u, 'ko-');
% hold on
% plot(x2, uL, 'bx');
% plot(x2, uR, 'rx');
% hold off




end