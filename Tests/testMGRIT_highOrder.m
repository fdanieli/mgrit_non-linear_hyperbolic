%testMGRIT_highOrder checks effectiveness of applying MGRIT to various
%systems of conservation laws, when high-order reconstruction schemes are
%used. Used to populate Fig.8 and Fig.9
%
% Runs MGRIT with different configurations, which vary degree of
% reconstruction used and order of time-stepper used for different levels.
% See Tab "Initialisation" for input parameters
% Displays error convergence plots for each of these configurations, and
% saves convergence history data in an opportune file


clear all
close all
clc


%% Initialisation
% - space / time grid
L		= 1;		                   % spatial domain size
NX 	= 2^6;                     % number of (spatial) nodes (should have as a factor a large enough power of 2: see MGRIT parameters below)
x		= linspace( 0, L, NX+1 );	 % (spatial) grid points
dx  = x(2)-x(1);
x		= x(1:NX)' + dx/2;
T   = [0,0.5];
NT  = 400;		                 % number of (temporal) nodes (should have as a factor a large enough power of 2: see MGRIT parameters below)


% - problem - Change problem here: possibilities are Burgers1D, shallowWater1D, euler1D, 
pb = @(u,t,ord,fType) Burgers1D(u,t,L,ord,fType);
% pb = @(u,t,ord,fType) shallowWater1D(u,t,L,ord,fType);
% pb = @(u,t,ord,fType) euler1D(u,t,L,ord,fType);
pbName = eraseBetween( eraseBetween( func2str(pb),'@',')', 'Boundaries','inclusive' ), '(',')', 'Boundaries', 'inclusive');

% - initial condition
switch pbName
	case 'Burgers1D'
		fInit = @(x) 4/3*sin(2*pi*x/L);
		u0 = fInit(x);

%     % to check CFL, first compute the serial solution and then run
% 	  CFL = max(max(abs(uS)))*dT(end)/dx		% CFL condition (should be <1)
		
		
	case 'shallowWater1D'
		fInit = @(x) [ ( 1+0.5*sin(2*pi*x/L) )/11 ;...
			             zeros(size(x))          ];
		u0 = fInit(x);
		
%     % to check CFL, first compute the serial solution and then run
%     dx = 2/NX;
%     g = 9.8;
% 		h = uS(  1:size(uS,1)/2                  , : );
% 		m = uS( (1:size(uS,1)/2) +   size(uS,1)/2, : );
% 		v = m./h;
% 		v( h==0 ) = 0;												% avoid division by 0
% 		c = sqrt( g*h );                   		% speed of sound
% 		
% 		CFL = max(max(abs(v)+c))*dT(end)/dx		% CFL condition (should be <1)
		
	
	case 'euler1D'
		fInit = @(x) [ ones(size(x)) ;...
			             zeros(size(x));
									 1+0.5*sin(2*pi*x/L)];
		u0 = fInit(x);

% 		% to check CFL, first compute the serial solution and then run
%     dx = 3/NX;
%     g = 5/3;
% 		r = uS(  1:size(uS,1)/3                  , : );
% 		m = uS( (1:size(uS,1)/3) +   size(uS,1)/3, : );
% 		v = m./r;
% 		v( r==0 ) = 0;										% avoid division by 0
% 		E = uS( (1:size(uS,1)/3) + 2*size(uS,1)/3, : );
% 		p = (g-1) * ( E - 0.5*r.*v.*v );
% 		c = sqrt( g*p./r );
% 		c( r==0 ) = 0;										% avoid division by 0
% 		
% 		CFL = max(max(abs(v)+c))*dT(end)/dx		% CFL condition (should be <1)

end


% - set parameters for MGRIT
NT	= [ NT; NT/2; NT/4 ];   % number of solver steps for level
dt  = ( T(2)-T(1) ) ./ NT;
maxIT = 20;	                % number of iterations
printIt = 1:maxIT;          % iterations to print
cycleType.name = 'V-cycle';
cycleType.it  = [1,0,1];
cycleType.FCF = false;
Solver  = cell(size(NT,1),1);
for i=1:size(NT,1)
	Solver{i} = @(u0,F,dT,numIT,t0) SSPRK3( u0,F,dT,numIT,t0 );
end
sSol = true;


% error evolution for each parameter combination
maxOrder = 3;
fTypes = {'LF', 'ROE'};

error = @(delta) sqrt( sum( sum( delta.^2 ) )       * dx * dt(1) );		% L2 norm in space-time
% error = @(delta) sqrt(      sum( delta(:,end,:).^2 )* dx         );		% L2 norm of final value
% error = @(delta)  max( max( abs( delta ) ) );		                      % Linf norm in space-time
% error = @(delta)  max(      abs( delta(:,end,:) ) );	             	  % Linf norm of final value

MGRITerrSame   = zeros( maxIT+1, maxOrder+1, length(fTypes) );
MGRITerrDecr   = zeros( maxIT+1, length(fTypes) );
MGRITerrIncr   = zeros( maxIT+1, length(fTypes) );
MGRITerrDecrST = zeros( maxIT+1, length(fTypes) );
MGRITerrIncrST = zeros( maxIT+1, length(fTypes) );



%% Simulations
% Same order for all levels
for order = 0:maxOrder
	for tp = 1:length(fTypes)
		
		% - problem definition
		fType = fTypes{tp};
		problem = @(u,t) pb(u,t,order,fType);
		Problem = cell(size(NT,1),1);
		for i=1:size(NT,1)
			Problem{i}= @(u,t) problem(u,t);
		end

		% launch MGRIT
		[ uOut , t, uS  ] = MGRIT_fas( Problem, Solver, T, NT, maxIT, cycleType, u0, printIt, sSol );
		
		% compute error (wrt serial)
		err = uOut - uS;
		MGRITerrSame(2:end, order+1, tp ) = error(err);
		
	end
end
% normalise
diff0 = [ zeros(size(uS,1),1), uS(:,2:end) ];
MGRITerrSame(1, :, :) = error(diff0); MGRITerrSame   = MGRITerrSame   ./ MGRITerrSame(1, :, :);




% Order increasing going coarser
for tp = 1:length(fTypes)

	% - problem definition
	fType = fTypes{tp};
	problem = @(u,t,ord) pb(u,t,ord,fType);
	Problem = cell(size(NT,1),1);
	for i=1:size(NT,1)
		Problem{i}= @(u,t) problem(u,t,i-1);
	end

	% launch MGRIT
	[ uOut , t, uS  ] = MGRIT_fas( Problem, Solver, T, NT, maxIT, cycleType, u0, printIt, sSol );
	
	err = uOut - uS;
	MGRITerrIncr(2:end, tp ) = error(err);
	
end
% normalise
diff0 = [ zeros(size(uS,1),1), uS(:,2:end) ];
MGRITerrIncr(1, : )   = error(diff0); MGRITerrIncr   = MGRITerrIncr   ./ MGRITerrIncr(1, :);


% Order decreasing going coarser
for tp = 1:length(fTypes)

	% - problem definition
	fType = fTypes{tp};
	problem = @(u,t,ord) pb(u,t,ord,fType);
	Problem = cell(size(NT,1),1);
	for i=1:size(NT,1)
		Problem{i}= @(u,t) problem(u,t,3-i);
	end

	% launch MGRIT
	[ uOut , t, uS  ] = MGRIT_fas( Problem, Solver, T, NT, maxIT, cycleType, u0, printIt, sSol );
	
	err = uOut - uS;
	MGRITerrDecr(2:end, tp ) = error(err);
	
end
% normalise
diff0 = [ zeros(size(uS,1),1), uS(:,2:end) ];
MGRITerrDecr(1, : )   = error(diff0); MGRITerrDecr   = MGRITerrDecr   ./ MGRITerrDecr(1, :);




% both time and space order increasing going coarser
for tp = 1:length(fTypes)

	% - problem definition
	fType = fTypes{tp};
	problem = @(u,t,ord) pb(u,t,ord,fType);
	Problem = cell(size(NT,1),1);
	for i=1:size(NT,1)
		Problem{i}= @(u,t) problem(u,t,i-1);
	end
	Solver    = cell(3,1);
	Solver{1} = @(u0,F,dT,numIT,t0) ForwardEuler( u0,F,dT,numIT,t0 );
	Solver{2} = @(u0,F,dT,numIT,t0) SSPRK2( u0,F,dT,numIT,t0 );
	Solver{3} = @(u0,F,dT,numIT,t0) SSPRK3( u0,F,dT,numIT,t0 );

	% launch MGRIT
	[ uOut , t, uS  ] = MGRIT_fas( Problem, Solver, T, NT, maxIT, cycleType, u0, printIt, sSol );
	
	err = uOut - uS;
	MGRITerrIncrST(2:end, tp ) = error(err);
	
end
% normalise
diff0 = [ zeros(size(uS,1),1), uS(:,2:end) ];
MGRITerrIncrST(1, : ) = error(diff0); MGRITerrIncrST = MGRITerrIncrST ./ MGRITerrIncrST(1, :);



% Both time and space order decreasing going coarser
for tp = 1:length(fTypes)

	% - problem definition
	fType = fTypes{tp};
	problem = @(u,t,ord) pb(u,t,ord,fType);
	Problem = cell(size(NT,1),1);
	for i=1:size(NT,1)
		Problem{i}= @(u,t) problem(u,t,3-i);
	end
	Solver    = cell(3,1);
	Solver{3} = @(u0,F,dT,numIT,t0) ForwardEuler( u0,F,dT,numIT,t0 );
	Solver{2} = @(u0,F,dT,numIT,t0) SSPRK2( u0,F,dT,numIT,t0 );
	Solver{1} = @(u0,F,dT,numIT,t0) SSPRK3( u0,F,dT,numIT,t0 );
	
	% launch MGRIT
	[ uOut , t, uS  ] = MGRIT_fas( Problem, Solver, T, NT, maxIT, cycleType, u0, printIt, sSol );
	
	err = uOut - uS;
	MGRITerrDecrST(2:end, tp ) = error(err);
	
end
% normalise
diff0 = [ zeros(size(uS,1),1), uS(:,2:end) ];
MGRITerrDecrST(1, : ) = error(diff0); MGRITerrDecrST = MGRITerrDecrST ./ MGRITerrDecrST(1, :);






%% fancy plots
figure
subplot(3,2,1)
semilogy(MGRITerrSame(:,:,1))
ylim([1e-16, 10])
legend(cellstr('w'+string((0:maxOrder)')))
xlabel('#it')
ylabel('err')
title( 'LF-same' )

subplot(3,2,2)
semilogy(MGRITerrSame(:,:,2))
ylim([1e-16, 10])
xlabel('#it')
ylabel('err')
legend(cellstr('w'+string((0:maxOrder)')))
title( 'ROE-same' )

subplot(3,2,3)
semilogy(MGRITerrIncr(:,1))
hold on
semilogy(MGRITerrDecr(:,1))
hold off
ylim([1e-16, 10])
legend({'Incr', 'Decr'})
xlabel('#it')
ylabel('err')
title( 'LF-weno order inc/dec going coarser' )

subplot(3,2,4)
semilogy(MGRITerrIncr(:,2))
hold on
semilogy(MGRITerrDecr(:,2))
hold off
ylim([1e-16, 10])
legend({'Incr', 'Decr'})
xlabel('#it')
ylabel('err')
title( 'ROE-weno order inc/dec going coarser' )

subplot(3,2,5)
semilogy(MGRITerrIncrST(:,1))
hold on
semilogy(MGRITerrDecrST(:,1))
hold off
ylim([1e-16, 10])
legend({'Incr', 'Decr'})
xlabel('#it')
ylabel('err')
title( 'LF-weno & SSPRK order inc/dec going coarser' )

subplot(3,2,6)
semilogy(MGRITerrIncrST(:,2))
hold on
semilogy(MGRITerrDecrST(:,2))
hold off
ylim([1e-16, 10])
legend({'Incr', 'Decr'})
xlabel('#it')
ylabel('err')
title( 'ROE-weno & SSPRK order inc/dec going coarser' )


% save data
for tp = 1:length(fTypes)
	fType = fTypes{tp};
	out = [ (1:size(MGRITerrSame,1))', MGRITerrSame(:,:,tp), MGRITerrIncr(:,tp), MGRITerrDecr(:,tp), MGRITerrIncrST(:,tp), MGRITerrDecrST(:,tp)]';
	filename = strcat('./results/MGRIT_',pbName,'WENO_',fType,'.dat');
	format = ['%4.d ', repmat(' %20.18f', [1,size(MGRITerrSame,2)+2+2] ), '\n' ];
	fileID = fopen(filename,'w');
	fprintf(fileID,format,out);
	fclose(fileID);
end





