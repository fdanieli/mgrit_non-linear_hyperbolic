%testMGRIT_highOrderCFL checks effectiveness of applying MGRIT to various
%systems of conservation laws, when high-order reconstruction schemes are
%used, and CFL condition is progressively degraded
% Used to populate Fig.7
%
% Runs MGRIT with different configurations, which vary degree of
% reconstruction used and refinement level of the time grid.
% See Tab "Initialisation" for input parameters
% Displays error convergence plots for each of these configurations, and
% saves convergence history data in an opportune file



clear all
close all
clc


%% Initialisation
% - space / time grid
L		= 1;		                  % spatial domain size
NX 	= 2^7;                    % number of (spatial) nodes (should be a power of 2)
x		= linspace( 0, L, NX+1 );	% (spatial) grid points
dx  = x(2)-x(1);
x		= x(1:NX)' + dx/2;
T   = [0,0.475];


% - problem - Change problem here: possibilities are Burgers1D, shallowWater1D, euler1D, 
pb = @(u,t,ord,fType) Burgers1D(u,t,L,ord,fType);
% pb = @(u,t,ord,fType) shallowWater1D(u,t,L,ord,fType);
% pb = @(u,t,ord,fType) euler1D(u,t,L,ord,fType);
pbName = eraseBetween( eraseBetween( func2str(pb),'@',')', 'Boundaries','inclusive' ), '(',')', 'Boundaries', 'inclusive');

% - initial condition
switch pbName
	case 'Burgers1D'
		fInit = @(x) sin(2*pi*x/L);
		u0 = fInit(x);
	case 'shallowWater1D'
		fInit = @(x) [ ( 1+0.5*sin(2*pi*x/L) )/8 ;...
			             zeros(size(x))          ];
		u0 = fInit(x);
	case 'euler1D'
		fInit = @(x) [ ones(size(x)) ;...
			             zeros(size(x));
									 ( 1+0.5*sin(2*pi*x/L) )/2];
		u0 = fInit(x);
end


% - set parameters for MGRIT
Nlvl   = 5;
maxNTs = 2.^(9:12);
maxIT = 20;         	   % number of iterations
printIt = 1:maxIT;       % iterations to print
cycleType.name = 'V-cycle';
cycleType.it  = [1,0,1];
cycleType.FCF = false;
sSol = true;

Solver  = cell(Nlvl,1);
for i=1:Nlvl
	Solver{i} = @(u0,F,dT,numIT,t0) SSPRK3( u0,F,dT,numIT,t0 );
end
% error evolution for each parameter combination
maxOrder = 2;
fType = 'ROE';

MGRITerr = zeros( maxIT+1, maxOrder+1, length(maxNTs) );



%% Simulations
for order = 0:maxOrder
	for i = 1:length(maxNTs)
		NT = maxNTs(i)./(2.^(0:Nlvl-1)');
		dt  = ( T(2)-T(1) ) ./ NT;
		
% 		if strcmp( pbName, 'Burgers1D' )
% 			CFL = max(abs(u0))*max(dt)/dx
% 		end
		
		error = @(delta) sqrt( sum( sum( delta.^2 ) )       * dx * dt(1) );		% L2 norm in space-time
		% error = @(delta) sqrt(      sum( delta(:,end,:).^2 )* dx         );		% L2 norm of final value
		% error = @(delta)  max( max( abs( delta ) ) );		                      % Linf norm in space-time
		% error = @(delta)  max(      abs( delta(:,end,:) ) );	             	  % Linf norm of final value		

		% - problem definition
		problem = @(u,t) pb(u,t,order,fType);
		Problem = cell(size(NT,1),1);
		for j=1:size(NT,1)
			Problem{j}= @(u,t) problem(u,t);
		end

		% launch MGRIT
		[ uOut , t, uS  ] = MGRIT_fas( Problem, Solver, T, NT, maxIT, cycleType, u0, printIt, sSol );
		
		% compute error (wrt serial)
		err = uOut - uS;
		MGRITerr( 2:end, order+1, i ) = permute( error(err), [3,2,1] );
		% normalise error
		MGRITerr(     1, order+1, i ) = error( [ zeros(size(uS,1),1), uS(:,2:end) ] );
		MGRITerr(     :, order+1, i ) = MGRITerr( :, order+1, i ) ./MGRITerr( 1, order+1, i );

		disp(strcat( 'order: ',int2str(order),'; NTmax=', int2str(maxNTs(i)) ))
		
	end
end








%% fancy plots

titlestr = strcat(cycleType.name, '(v=',int2str(cycleType.it(1)),',w=',int2str(cycleType.it(2)),') NX', int2str(NX(1)) );
append = cellstr( 's'+string(2*(0:maxOrder)+1) ); %{'s1', 's3', 's5'};
lgnd = cellstr('NT='+string(maxNTs'));

figure
for i=1:size(MGRITerr,2)
	subplot(1,size(MGRITerr,2),i)
	semilogy(permute(MGRITerr(:,i,:),[1,3,2]));
	axis([0,maxIT,1e-16,1e1])

	xlabel('Iteration')
	ylabel('Error')
	title(strcat( titlestr, ' - ', append{ i } ))
	legend(lgnd);
end



% save data
out = [ (1:size(MGRITerr,1))', reshape(MGRITerr,[ size(MGRITerr,1), size(MGRITerr,2)*size(MGRITerr,3) ] )]';
filename = strcat('./results/MGRIT_CFL_',pbName,'_WENO',fType,'_Nlvl',int2str(Nlvl),'_NX',int2str(NX),'_maxNT',int2str(max(maxNTs)),'_maxS', int2str(2*maxOrder+1),'.dat');
format = ['%4.d ', repmat(' %20.18f', [1,size(MGRITerr,2)*size(MGRITerr,3)] ), '\n' ];
fileID = fopen(filename,'w');
fprintf(fileID,format,out);
fclose(fileID);





