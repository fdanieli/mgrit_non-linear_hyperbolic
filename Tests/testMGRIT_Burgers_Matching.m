%testMGRIT_Burgers_Matching checks effectiveness of applying MGRIT to
%Burgers' equations when fine-integrator matching techniques are employed
%to improve approximation of Schur complement.
%Used to populate Fig.5. Also used to populate Fig.6: in that case, line
%419 of MGRIT_fas should be commented out.
%
% Runs MGRIT with different configurations, which vary degree at which the
% coarse integrator matches the fine integrator.
% See Tab "Initialisation" for input parameters, and Tab "MGRIT" for
% multigrid-specific parameters.
% Displays error convergence plots for each of these configurations, and
% saves convergence history data in an opportune file

clear all
close all
clc


%% Initialisation
maxIT = 20;	  % number of iterations
po2s  = 6:10;	% refinements of spatial grid
nlvl  = 5;    % time grid levels
shift = 3;    % this identifies refinements of temporal grid: it's just a
              %  handy variable to easily modify the CFL number at lowest
              %  level. If shift = nlvl-2, then the CFL is ~0.95
IC = 'stat';  % type of IC: stationary or moving shock
% IC = 'mov';

MGRITerr = zeros( maxIT+1, 3, length(po2s) );

for po2 = po2s
	% - space / time grid
	L		= 1;		% spatial domain size

	NX 	= 2^po2;	% number of (spatial) nodes
	dx  = L/NX;
	x   = ((dx/2):dx:(L-dx/2))';			% these are actually the cell *centers*

	T   = [0,0.475];  % time domain

	% - initial condition in space
	if strcmp( IC, 'mov' )
		u0 = 0.5+0.5*sin(2*pi*x/L);
	else
		u0 = sin(2*pi*x/L);
	end

	% problem parameters
	Problem = @(u,t,dt,OMatch,lvl) Burgers1D_LF_corr(u,t,L,dt,OMatch,lvl);


	%% MGRIT
	% - set parameters for MGRIT
	NT	= 2.^((po2+shift):-1:(po2+shift-nlvl+1))';      % number of solver steps for level
	DT	= (T(2)-T(1))./NT;                              % size of time step per level
% 	CFL = max(abs(u0))*DT'/dx			                      % CFL condition (should be <1)
	printIt = 1:maxIT;                                  % iterations to print
	cycleType.name = 'V-cycle';
	cycleType.it   = [1,0,1];
	cycleType.FCF  = false;

	error = @(delta) sqrt( sum( sum( delta.^2 ) )       * dx * min(DT) );		% L2 norm in space-time
	% error = @(delta) sqrt(      sum( delta(:,end,:).^2 )* dx         );		% L2 norm of final value
	% error = @(delta)  max( max( abs( delta ) ) );		                      % Linf norm in space-time
	% error = @(delta)  max(      abs( delta(:,end,:) ) );	             	  % Linf norm of final value		



	corrLvl = -1:1;



	for corr = corrLvl
		% - set solvers and problems
		Solvers  = cell(size(NT,1),1);
		Problems = cell(size(NT,1),1);
		for i=1:size(NT,1)
			Solvers{ i} = @(u0,F,dT,numIT,t0) ForwardEuler( u0,F,dT,numIT,t0 );
			Problems{i} = @(u,t) Problem( u,t,(T(2)-T(1))/NT(i),corr, i );
		end
		Problems{1} = @(u,t) Problem( u,t,(T(2)-T(1))/NT(1),-1,1 );
		% - compute serial solution
		sSol = true;


		%% Simulations
		% launch MGRIT
		[ uOut , t, uS, err ] = MGRIT_fas( Problems, Solvers, T, NT, maxIT, cycleType, u0, printIt, sSol );
	
		% normalise error
		MGRITerr( 2:end, corr-corrLvl(1)+1, po2-po2s(1)+1 ) = permute( error(uS - uOut), [3,2,1] );
		MGRITerr(     1, corr-corrLvl(1)+1, po2-po2s(1)+1 ) = error( [ zeros(size(uS,1),1), uS(:,2:end) ] );
		MGRITerr(     :, corr-corrLvl(1)+1, po2-po2s(1)+1 ) = MGRITerr( :, corr-corrLvl(1)+1, po2-po2s(1)+1 ) ...
																											  ./MGRITerr( 1, corr-corrLvl(1)+1, po2-po2s(1)+1 );

	end
	
end


%% Plotting

titlestr = strcat(cycleType.name, ' nlvl', int2str(length(NT)), ' CFLmax', num2str(max(CFL),2), ' F');
if cycleType.FCF
	titlestr = strcat(titlestr, '-CF');
end
titlestr = strcat(titlestr, 'smooth');
append = {'No match', '0th ord', '1st ord' };
lgnd = cellstr('2^{'+string(po2s') +'}x'+'2^{'+string(po2s'+shift)+'}');


figure
for i=1:size(MGRITerr,2)
	subplot(1,size(MGRITerr,2),i)
	semilogy(permute(MGRITerr(:,i,:),[1,3,2]));
	axis([0,maxIT,1e-16,1e1])
	% fancy labels
	xlabel('Iteration')
	ylabel('Error')
	title(strcat( titlestr, ' - ', append{ corrLvl(i)+2 } ))
	legend(lgnd);
end
	
% save fig
titlestr = strcat('./results/testBurgers_IC',IC,'_',strrep(strrep(titlestr,' ','_'),'.','p'));
savefig(strcat(titlestr, '.fig'))
% save error
filename = strcat(titlestr,'.dat');
out = [ (1:size(MGRITerr,1))', reshape(MGRITerr,[size(MGRITerr,1),size(MGRITerr,2)*size(MGRITerr,3)]) ];
save( filename, 'out', '-ascii' );









