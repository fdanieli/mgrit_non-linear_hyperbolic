## Multigrid Reduction in Time for non-linear hyperbolic equations

This repository contains the files necessary to run the simulations included in the article "Multigrid Reduction in Time for non-linear hyperbolic equations".

    home
    ├── Integrators             # Time-stepping routines
    │   └── ...
    ├── MGRIT                   # Implementation of MGRIT
    │   └── ...
    ├── Problems                # Discretisation of the equations
    │   ├── WENO                # 	Implementation of the WENO scheme
    │   └── ...
    ├── Tests                   # Scripts to recover the data used in plots
    │   └── ...
    └── ...



