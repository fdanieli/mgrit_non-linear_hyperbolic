function [ u ] = ForwardEuler( u0,F,dT,numIT,t0 )
%ForwardEuler Implementation of Forward Euler integration method.
%
% Syntax:  [ u ] = ForwardEuler( u0,F,dT,numIT,t0 )
%
% Inputs:
%    u0    - Vector of initial conditions
%    F     - Function of u and t. Right-hand side of ODE
%    dT    - Scalar. Size of time step (fixed)
%    numIT - Number of iterations for the algorithm
%    t0    - Initial time
%
% Outputs:
%    u     - Solution matrix. Different columns correspond to different
%             times
%
%
% Author: Federico Danieli, Numerical Analysis Group
% University of Oxford, Dept. of Mathematics
% email address: federico.danieli@maths.ox.ac.uk  
% June 2017; Last revision: Jan-2018
%

u = zeros( size(u0,1), numIT+1 );
u(:,1) = u0;


for i=1:numIT
	k1 = F( u(:,i), t0 + (i-1)*dT );

	u(:,i+1) = u(:,i) + dT *  k1 ;
end

end

