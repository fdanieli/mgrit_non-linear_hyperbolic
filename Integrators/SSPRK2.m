function [ u ] = SSPRK2( u0,F,dT,numIT,~ )
%SSPRK2 Implementation of the 2nd order Strong Stability-Preserving Runge-Kutta integration method.
%
% Strong Stability-Preserving methods are designed with applications to
% hyperbolic equations in mind. They are high-order methods which provide a
% TVD solution. More info can be found in:
% [1] "Efficient Implementation of Essentially Non-oscillatory
%      Shock-Capturing Schemes", CW Shu, S Osher
%      (https://pdf.sciencedirectassets.com/272570/1-s2.0-S0021999100X02252/1-s2.0-0021999188901775/main.pdf?x-amz-security-token=AgoJb3JpZ2luX2VjECkaCXVzLWVhc3QtMSJGMEQCIEBp8ACbWPicOsa5iKimVityPxI%2BvDPgnA2uEqXuud8oAiAIpHbifftLrwFhBOmB7H8yLj9VavHTkVkeVSqJAqjtdCraAwgxEAIaDDA1OTAwMzU0Njg2NSIMc7p6Oqq8MgMQ4fjMKrcDvpLZipyJO6TmjRjW%2B536%2FvcQ04Xj5wxmS5DJExpZGw7oE24YgVX2ggdStzWoeYkTrQMsGL0yUJ7yUQQnH%2BLqO3%2FfnCK55gP6uSHqjWCKCCcdtrcF3ewLu%2FW83d7ZerGDzZ0fdH%2FmjyVHAABMQ%2Bu6F71dqDMp0dWsQk4bOi5rdGewq6%2BpLAKlASTgsAKJbS3p%2BCttOKoFxphhgtGOOEnGwBszY3tLw0RhjbPvMFMmb%2BLg0PezzrwuLDEgHdZGKJXumAGACjbf%2FQ%2FMGJYmqG3jJRCUzBDdK4WD0M%2BayVx9FcLc%2BTx3CXJkBlobpq2vytZ42wb6rQkU2B1p%2BDWtc6jSAQcBja%2B72qBPRNRaIjwMUfRyY9yEIgok%2FAev2a%2Fgauu8g5AEpWRaBhmL%2F4IPo67KjTfxuEKWxhGPXGxfuGm6ORObrftRn84OXv3Yds7wNY6zlRpZdDfesj0uXckxepykBSnbPf8VVToh2yYB9CxFXRx84l7B%2F%2Fs3udbvU2sgU3DBNHx117gTcDGMCWJxnm%2F68Z5zd3%2FSDA2Ki%2FmYMt3U6ZE62lmOnOKqkVYfC%2BvAG%2Fqgbx9NWOWwETCIvfvmBTq1AQviBCh17tWJMQ1Ur6v5Kxxa1EmwJ0kLbTkLa9bodGYTfhDJrbBz5Z2XRC8YhH5SXwnZ7r0jfc5V0%2F3VxuYU4pj4kRuMIATxcjTf9y6DgAD5qNiwZwILUbImVLTxLcXbJ3%2Fxy57H2hFnCfwTJSgi1JB1RdVqK4bEZHjWsfYBh%2Fqkd%2FaD1k0TwyxYCxg4L9leaSXzvfzeRtMyz1iz%2B8X4VsRu%2BOlSOoX%2BHkYcE2Sff6Ob5RX2mhw%3D&AWSAccessKeyId=ASIAQ3PHCVTY46ZIPNNO&Expires=1558115847&Signature=aYJQE3b6s%2FXBy%2FkDoDyv5Z%2BFT3k%3D&hash=10df3575df401b2c5a72d2f82b2c857ecfcba0da06936419063d6f8127da43c8&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=0021999188901775&tid=spdf-3d9a2d28-8515-49eb-8bd5-43bb0a4b2702&sid=3160195195b3664409684b091b9306bbcb74gxrqa&type=client)
% [2] "Strong Stability-Preserving High-Order Time Discretization Methods" 
%      S Gottlieb, CW Shu, E Tadmor
%      (https://epubs.siam.org/doi/pdf/10.1137/S003614450036757X)
%
% Note: the right-hand side F should not depend on time. Not because SSPRK
% methods don't support this, but just because I haven't found such
% formulation yet.
%
%
% Syntax:  [ u ] = SSPRK2( u0,F,dT,numIT,t0 )
%
% Inputs:
%    u0    - Vector of initial conditions
%    F     - Function handle. Right-hand side of ODE
%    dT    - Scalar. Size of time step (fixed)
%    numIT - Number of iterations for the algorithm
%    t0    - Initial time (unused)
%
% Outputs:
%    u     - Solution matrix. Different columns correspond to different
%             times

u = zeros( size(u0,1), numIT+1 );
u(:,1) = u0;


for i=1:numIT
	u1       =   u(:,i)      + dT*F( u(:,i), [] );

	u(:,i+1) = ( u(:,i) + u1 + dT*F( u1    , [] ) ) / 2;

end

end

